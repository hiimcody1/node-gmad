node-gmad
==========

GMAD Extractor implemented in Node.js

Installation
------------

    npm install node-gmad

Usage
------

node-gmad makes use of the Q library to provide promises and callback support.
You can call it either way, though examples will use promises.

```js
var GMAD = require('node-gmad');

GMAD.parseAddon("path/to/gma/file").then(function(gmaFile) {
    //see GMAFile structure below
    GMAD.extractFileFromAddon("base/path",gmaFile,idOfFile).then(function(extractionStatus) {

    });
});
```

Methods
--------

- [parseAddon](#parseAddon)
- [extractFileFromAddon](#extractFileFromAddon)

### parseAddon ###

**parseAddon(filePath)**


Parse and return the GMA file.

Options:  
filePath (string): The path to the GMA file  

### extractFileFromAddon ###

**extractFileFromAddon(basePath, gmaFile, fileIndex)**


Extract a file from the GMA file

Options:  
basePath (string): The base directory to extract the file  
gmaFile (GMAFile): The parsed GMA file  
fileIndex (int): The index of the file you wish to extract  

GMAFile Structure
------------------

```js
var GMAFile = function() {
    this.header = {
        "identifier": "",   //Should always be GMAD
        "version": ""       //Always 3 according to GMAD
    }
    //Metadata
    this.steamId        =   new Long(); //SteamId of the addon creator
    this.timestamp      =   new Long(); //Unix timestamp
    this.unusedString   =   "";         //String data, presently unused
    this.name           =   "";         //Name of the Addon
    this.description    =   "";         //This may be JSON or a blurb containing bbcode
    this.author         =   "";         //Doesn't appear to be set properly, always "author" or "Author Name"
    this.version        =   "";         //Addon version, GMAD states its not used right now

    //Filedata
    this.fileDataOffset =   new Long(); //Offset where file data starts
    this.fileList       =   [];         //Array of GMAFileEntry
}
```

GMAFileEntry Structure
-----------------------

```js
var GMAFileEntry = function() {
    this.title          =   "";             //File path
    this.size           =   new Long();     //Size of file
    this.crc            =   new Long();     //CRC of file
    this.offset         =   new Long();     //Offset indicating where file data starts
    this.fileNumber     =   0;              //Number in list @TODO Do we even need this?
}
```

ExtractionStatus Structure
---------------------------

```js
var ExtractionStatus = function() {
    this.source         =   "";         //Source file
    this.destination    =   "";         //Destination file
    this.success        =   false;      //Whether extraction was successful
    this.message        =   "";         //Message returned from extraction function

    this.stats = {
        "bytesRead": 0,                 //Total bytes read from source file
        "bytesWritten": 0               //Total bytes writen to destination file
    }
}
```

### Changelog ###

* 1.0.1 Add Readme

* 1.0.0 Initial Release